$(document).ready(function(){
	$('.c-header__toggle input:checkbox').change(function(){
	    if($(this).is(":checked")) {
	    	$('.js-overlays01').css({zIndex: "10"});
	    	$('.js-overlays02').css({zIndex: "10"});
	        setTimeout(function(){
	        	$('.c-header__canvas').css({zIndex: "11"});
	        	$('.c-header__inner').css({zIndex: "99"});
	        	$('.c-header__canvas, .c-header__inner').addClass("is-open"); 
	    	}, 750);
	    } else {
	        $('.c-header__canvas, .c-header__inner').removeClass("is-open").css({zIndex: "0"});
	        $('.js-overlays01').css({zIndex: "0"});
	    	$('.js-overlays02').css({zIndex: "0"});
	    }
	});

	setTimeout(function(){
	    $('.c-loading').fadeOut();
	}, 2800);
});






(function($) {
	var points = [];
	var rafID = null;

	var guiVars = function() {
		this.totalPoints = 10;
		this.viscosity = 20;
		this.mouseDist = 80;
		this.damping = 0.15;
		this.showIndicators = false;
		this.leftColor = 'rgb(249, 151, 151)';
		this.rightColor = 'rgb(250, 171, 171)';
	}
	var vars = new guiVars();

	var mouseX = 0,
	mouseY = 0,
	mouseLastX = 0,
	mouseLastY = 0,
	mouseDirectionX = 0,
	mouseDirectionY = 0,
	mouseSpeedX = 0,
	mouseSpeedY = 0;

	function mouseDirection(e) {
		if (mouseX < e.pageX)
			mouseDirectionX = 1;
		else if (mouseX > e.pageX)
			mouseDirectionX = -1;
		else
			mouseDirectionX = 0;

		if (mouseY < e.pageY)
			mouseDirectionY = 1;
		else if (mouseY > e.pageY)
			mouseDirectionY = -1;
		else
			mouseDirectionY = 0;

		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	$(document).on('mousemove', mouseDirection);

	function mouseSpeed() {
		mouseSpeedX = mouseX - mouseLastX;
		mouseSpeedY = mouseY - mouseLastY;

		mouseLastX = mouseX;
		mouseLastY = mouseY;

		setTimeout(mouseSpeed, 50);
	}
	mouseSpeed();

	function Point(x, y, canvas) {
		this.x = x;
		this.ix = x;
		this.vx = 0;
		this.cx = 0;
		this.y = y;
		this.iy = y;
		this.cy = 0;
		this.canvas = canvas;
	}

	Point.prototype.move = function() {
		this.vx += (this.ix - this.x) / vars.viscosity;

		var dx = this.ix - mouseX,
		dy = this.y - mouseY;

		var gap = this.canvas.data('gap');

		if ((mouseDirectionX > 0 && mouseX > this.x) || (mouseDirectionX < 0 && mouseX < this.x)) {
			if (Math.sqrt(dx * dx) < vars.mouseDist && Math.sqrt(dy * dy) < gap) {
				this.vx = mouseSpeedX / 8
			}
		}

		this.vx *= (1 - vars.damping);
		this.x += this.vx;
	};

	function canvasNavi() {
		var canvas = $('#canvasNavi');
		var context = canvas.get(0).getContext('2d');

		cancelAnimationFrame(rafID);

		$('#canvasNavi').get(0).width = $(window).width();
		$('#canvasNavi').get(0).height = $(window).height();

		points = [];
		var gap = (canvas.height()) / (vars.totalPoints - 1);
		var pointX = $(window).width() / 2;

		for (var i = 0; i <= vars.totalPoints - 1; i++)
			points.push(new Point(pointX, i * gap, canvas));

		renderCanvasNavi();

		canvas.data('gap', gap);
	}

	function renderCanvasNavi() {
		var canvas = $('#canvasNavi');
		var context = canvas.get(0).getContext('2d');

		rafID = requestAnimationFrame(renderCanvasNavi);

		context.clearRect(0, 0, canvas.width(), canvas.height());
		context.fillStyle = vars.leftColor;
		context.fillRect(0, 0, canvas.width(), canvas.height());

		for (var i = 0; i <= vars.totalPoints - 1; i++){
			points[i].move();
		}

		context.fillStyle = vars.rightColor;
		context.strokeStyle = vars.rightColor;
		context.lineWidth = 1;
		context.beginPath();

		context.moveTo($(window).width() /2, 0);

		for (var i = 0; i <= vars.totalPoints - 1; i++) {
			var p = points[i];

			if (points[i + 1] != undefined) {
				p.cx = (p.x + points[i + 1].x) / 2 - 0.0001;
				p.cy = (p.y + points[i + 1].y) / 2;
			} else {
				p.cx = p.ix;
				p.cy = p.iy;
			}

			context.bezierCurveTo(p.x, p.y, p.cx, p.cy, p.cx, p.cy);
		}

		context.lineTo($(window).width(), $(window).height());
		context.lineTo($(window).width(), 0);
		context.closePath();
		context.fill();

		if (vars.showIndicators) {
			context.fillStyle = '#000';
			context.beginPath();
			for (var i = 0; i <= vars.totalPoints - 1; i++) {
				var p = points[i];

				context.rect(p.x - 2, p.y - 2, 4, 4);
			}
			context.fill();

			context.fillStyle = '#fff';
			context.beginPath();
			for (var i = 0; i <= vars.totalPoints - 1; i++) {
				var p = points[i];

				context.rect(p.cx - 1, p.cy - 1, 2, 2);
			}
			context.fill();
		}
	}

	function resizeHandler() {
		canvasNavi();
	}
	$(window).on('resize', resizeHandler).trigger('resize');
})(jQuery);


(function($) {
	var vars = {
		pointsPerBox: 15,
		viscosity: 9,
		mouseDist: 70,
		damping: 0.1,
		showIndicators: false,
		overflow: 70
	};

	var rafID = null;

	var boxes = [{
		backgroundColor: '#f76c6c'
	}];

	var mouseX = 0,
	mouseY = 0,
	mouseLastX = 0,
	mouseLastY = 0,
	mouseDirectionX = 0,
	mouseDirectionY = 0,
	mouseSpeedX = 0,
	mouseSpeedY = 0;

	function mouseDirection(e) {
		if (mouseX < e.pageX)
			mouseDirectionX = 1;
		else if (mouseX > e.pageX)
			mouseDirectionX = -1;
		else
			mouseDirectionX = 0;

		if (mouseY < e.pageY)
			mouseDirectionY = 1;
		else if (mouseY > e.pageY)
			mouseDirectionY = -1;
		else
			mouseDirectionY = 0;

		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	$(document).on('mousemove', mouseDirection);

	function mouseSpeed() {
		mouseSpeedX = mouseX - mouseLastX;
		mouseSpeedY = mouseY - mouseLastY;

		mouseLastX = mouseX;
		mouseLastY = mouseY;

		setTimeout(mouseSpeed, 70);
	}
	mouseSpeed();

	function Point(x, y, axis, canvas) {
		this.x = x;
		this.initialx = x;
		this.vx = 0;
		this.controlX = 0;
		this.y = y;
		this.initialy = y;
		this.vy = 0;
		this.controlY = 0;
		this.$canvas = canvas;
		this.axis = axis;
	}

	Point.prototype.move = function() {
		this.vy += (this.initialy - this.y) / vars.viscosity;

		var deltax = this.initialx - mouseX;
		var deltay = this.initialy - mouseY;

		var gap = this.$canvas.data('gap');

		if ((mouseDirectionY > 0 && mouseY > this.y) || (mouseDirectionY < 0 && mouseY < this.y)) {
			if (Math.sqrt(deltax * deltax) < vars.mouseDist && Math.sqrt(deltay * deltay) < gap) {
				this.vy = mouseSpeedY / 8
			}
		}

		this.vy *= (1 - vars.damping);
		this.y += this.vy;
	};


	function initCanvas() {
		var $canvas = $('#canvasFull01');
		var $container = $canvas.parent();
		var context = $canvas.get(0).getContext('2d');

		cancelAnimationFrame(rafID);

		$canvas.get(0).width = $container.width();
		//$canvas.get(0).height = $container.height() + (vars.overflow * 2);
		$canvas.get(0).height = $container.height();

		var gap = ($container.width() / (vars.pointsPerBox - 1)) / boxes.length;
		var topY = vars.overflow;
		var bottomY = $canvas.height() - vars.overflow;

		boxes.forEach(function(box, index) {
			var boxWidth = $container.width() / boxes.length;
			box.points = {
				top: [],
				bottom: [],
			};

			for (var i = 0; i <= vars.pointsPerBox - 1; i++) {
				box.points.top.push(new Point((i * gap) + (index * boxWidth), topY, 'h', $canvas));
				box.points.bottom.push(new Point((i * gap) + (index * boxWidth), bottomY, 'h', $canvas));
			}
		});

		renderCanvas();
		$canvas.data('gap', gap);
	}

	function renderCanvas() {
		var $canvas = $('#canvasFull01');
		var $container = $canvas.parent();

		var gap = ($container.width() / (vars.pointsPerBox - 1)) / boxes.length;

		var canvasHeight = $canvas.height();
		var canvasWidth = $canvas.width();
		var containerWidth = $container.width();

		var context = $canvas.get(0).getContext('2d');

		rafID = requestAnimationFrame(renderCanvas);

		context.clearRect(0, 0, canvasWidth, canvasHeight);


		boxes.forEach(function(box, index) {
			box.points.top.forEach(function(point, i) {
				//point.move();
			});

			box.points.bottom.forEach(function(point, i) {
				point.move();
			});

			context.fillStyle = box.backgroundColor;
			context.strokeStyle = box.backgroundColor;
			context.lineWidth = 1;
			context.beginPath();

			var startX = containerWidth - (containerWidth / (index + 1));
			var endX = containerWidth;
			var topY = vars.overflow;
			var bottomY = canvasHeight - vars.overflow;

			context.moveTo(startX, vars.overflow);

			context.lineTo(startX, bottomY);

			var topPoints = box.points.top;
			var bottomPoints = box.points.bottom;

			for (var i = 0; i <= vars.pointsPerBox - 1; i++) {
				var point = bottomPoints[i];

				if (i == 0) {
					point.x = startX;
					point.y = bottomY;
					point.controlX = (point.x + bottomPoints[i + 1].x) / 2 - 0.0001;
					point.controlY = (point.y + bottomPoints[i + 1].y) / 2;
				} else if (i == vars.pointsPerBox - 1) {
					point.x = point.controlX = endX;
					point.y = point.controlY = bottomY;
				} else {
					point.controlX = (point.x + bottomPoints[i + 1].x) / 2 - 0.0001;
					point.controlY = (point.y + bottomPoints[i + 1].y) / 2;
				}

				context.bezierCurveTo(point.x, point.y, point.controlX, point.controlY, point.controlX, point.controlY);
			}

			context.lineTo(endX, vars.overflow);

			var ting = vars.pointsPerBox - 1;
			for (var j = ting; j >= 0; j--) {
				var point = topPoints[j];

				if (j == vars.pointsPerBox - 1) {
					point.x = endX;
					point.y = topY;
					point.controlX = (point.x + topPoints[j - 1].x) / 2 - 0.0001;
					point.controlY = (point.y + topPoints[j - 1].y) / 2;
				} else if (j == 0) {
					point.x = point.controlX = startX;
					point.y = point.controlY = topY;
				} else {
					point.controlX = (point.x + topPoints[j - 1].x) / 2 - 0.0001;
					point.controlY = (point.y + topPoints[j - 1].y) / 2;
				}

				context.bezierCurveTo(point.x, point.y, point.controlX, point.controlY, point.controlX, point.controlY);
			}

			context.closePath();
			context.fill();

			if (vars.showIndicators) {

				context.fillStyle = '#000';
				context.beginPath();
				for (var i = 0; i <= vars.pointsPerBox - 1; i++) {
					var point = topPoints[i];
					context.rect(point.x - 2, point.y - 2, 4, 4);

					point = bottomPoints[i];
					context.rect(point.x - 2, point.y - 2, 4, 4);
				}
				context.fill();

				context.fillStyle = '#fff';
				context.beginPath();

				for (var i = 0; i <= vars.pointsPerBox - 1; i++) {
					var point = topPoints[i];
					context.rect(point.cx - 1, point.cy - 1, 2, 2);
					point = bottomPoints[i];
					context.rect(point.cx - 1, point.cy - 1, 2, 2);
				}
				context.fill();
			}
		});
	}

	function resizeHandler() {
		initCanvas();
	}
	$(window).on('resize', resizeHandler).trigger('resize');
})(jQuery);

(function($) {
	var points = [];
	var rafID = null;

	var guiVars = function() {
		this.totalPoints = 15;
		this.viscosity = 20;
		this.mouseDist = 80;
		this.damping = 0.15;
		this.showIndicators = false;
		this.leftColor = 'rgba(0, 0, 0, 0)';
		this.rightColor = '#a8d0e6';
	}
	var vars = new guiVars();

	var mouseX = 0,
	mouseY = 0,
	mouseLastX = 0,
	mouseLastY = 0,
	mouseDirectionX = 0,
	mouseDirectionY = 0,
	mouseSpeedX = 0,
	mouseSpeedY = 0;

	function mouseDirection(e) {
		if (mouseX < e.pageX)
			mouseDirectionX = 1;
		else if (mouseX > e.pageX)
			mouseDirectionX = -1;
		else
			mouseDirectionX = 0;

		if (mouseY < e.pageY)
			mouseDirectionY = 1;
		else if (mouseY > e.pageY)
			mouseDirectionY = -1;
		else
			mouseDirectionY = 0;

		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	$(document).on('mousemove', mouseDirection);

	function mouseSpeed() {
		mouseSpeedX = mouseX - mouseLastX;
		mouseSpeedY = mouseY - mouseLastY;

		mouseLastX = mouseX;
		mouseLastY = mouseY;

		setTimeout(mouseSpeed, 50);
	}
	mouseSpeed();

	function Point(x, y, canvas) {
		this.x = x;
		this.ix = x;
		this.vx = 0;
		this.cx = 0;
		this.y = y;
		this.iy = y;
		this.cy = 0;
		this.canvas = canvas;
	}

	Point.prototype.move = function() {
		this.vx += (this.ix - this.x) / vars.viscosity;

		var dx = this.ix - mouseX,
		dy = this.y - mouseY;

		var gap = this.canvas.data('gap');

		if ((mouseDirectionX > 0 && mouseX > this.x) || (mouseDirectionX < 0 && mouseX < this.x)) {
			if (Math.sqrt(dx * dx) < vars.mouseDist && Math.sqrt(dy * dy) < gap) {
				this.vx = mouseSpeedX / 8
			}
		}

		this.vx *= (1 - vars.damping);
		this.x += this.vx;
	};

	function canvasFull02() {
		var canvas = $('#canvasFull02');
		var context = canvas.get(0).getContext('2d');

		cancelAnimationFrame(rafID);

		$('#canvasFull02').get(0).width = $(window).width();
		$('#canvasFull02').get(0).height = $(window).height();

		points = [];
		var gap = (canvas.height()) / (vars.totalPoints - 1);
		var pointX = ($(window).width() / 3)*2;

		for (var i = 0; i <= vars.totalPoints - 1; i++)
			points.push(new Point(pointX, i * gap, canvas));

		renderCanvasFull02();

		canvas.data('gap', gap);
	}

	function renderCanvasFull02() {
		var canvas = $('#canvasFull02');
		var context = canvas.get(0).getContext('2d');

		rafID = requestAnimationFrame(renderCanvasFull02);

		context.clearRect(0, 0, canvas.width(), canvas.height());
		context.fillStyle = vars.leftColor;
		context.fillRect(0, 0, canvas.width(), canvas.height());

		for (var i = 0; i <= vars.totalPoints - 1; i++){
			points[i].move();
		}

		context.fillStyle = vars.rightColor;
		context.strokeStyle = vars.rightColor;
		context.lineWidth = 1;
		context.beginPath();

		context.moveTo(($(window).width() /3)*2, 0);

		for (var i = 0; i <= vars.totalPoints - 1; i++) {
			var p = points[i];

			if (points[i + 1] != undefined) {
				p.cx = (p.x + points[i + 1].x) / 2 - 0.0001;
				p.cy = (p.y + points[i + 1].y) / 2;
			} else {
				p.cx = p.ix;
				p.cy = p.iy;
			}

			context.bezierCurveTo(p.x, p.y, p.cx, p.cy, p.cx, p.cy);
		}

		context.lineTo(0, $(window).height());
		context.lineTo(0, 0);
		context.closePath();
		context.fill();

		if (vars.showIndicators) {
			context.fillStyle = '#000';
			context.beginPath();
			for (var i = 0; i <= vars.totalPoints - 1; i++) {
				var p = points[i];

				context.rect(p.x - 2, p.y - 2, 4, 4);
			}
			context.fill();

			context.fillStyle = '#fff';
			context.beginPath();
			for (var i = 0; i <= vars.totalPoints - 1; i++) {
				var p = points[i];

				context.rect(p.cx - 1, p.cy - 1, 2, 2);
			}
			context.fill();
		}
	}

	function resizeHandler() {
		canvasFull02();
	}
	$(window).on('resize', resizeHandler).trigger('resize');
})(jQuery);
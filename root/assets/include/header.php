<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/style.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:700&display=swap" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
<script>document.documentElement.className = 'js';</script>
</head>
<body class="page-<?php echo $pageid; ?>">

<header class="c-header">
	<svg class="js-overlays01" viewBox="0 0 100 100" preserveAspectRatio="none">
		<path class="js-overlays__path"></path>
		<path class="js-overlays__path"></path>
	</svg>
	<svg class="js-overlays02" viewBox="0 0 100 100" preserveAspectRatio="none">
		<path class="js-overlays02__path"></path>
		<path class="js-overlays02__path"></path>
	</svg>
	<div class="c-header__logo">
		<span>wrk</span>
	</div>
	<div class="c-header__toggle">
		<label>
			<input type="checkbox">
			<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
				<path class="line--1" d="M0 40h62c13 0 6 28-4 18L35 35" />
				<path class="line--2" d="M0 50h70" />
				<path class="line--3" d="M0 60h62c13 0 6-28-4-18L35 65" />
			</svg>
		</label>
	</div>
	<div class="c-header__inner">				
		<div class="c-header__left">
			<div class="c-header__box">
				<h3 class="c-header__title">contact</h3>
				<p>hello@waaark.com</p>
				<ul class="c-header__social">
					<li><a href="">facebook</a></li>
					<li><a href="">twitter</a></li>
					<li><a href="">dribbble</a></li>
				</ul>
			</div>
		</div>
		<div class="c-header__right">
			<div class="c-header__box">
				<h3 class="c-header__title">navigation</h3>
				<ul class="c-header__nav">
					<li><a href="">Vision</a></li>
					<li><a href="">Works</a></li>
					<li><a href="">Studio</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="c-header__canvas"><canvas id="canvasNavi"></canvas></div>
</header>
